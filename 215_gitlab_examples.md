This document contains some examples of how the [Mermaid Graph Library](https://mermaid.js.org/) can be used inside GitLab Markdown documents:

## Simple Graph Example

```mermaid
graph TD
    A --> B
    A --> C
    C --> D
    C --> E
    E --> A
```

## Timeline

```mermaid
timeline
    title History of Programming Languages
    1957 : Fortran
    1972 : C
    1987 : Perl
    1990 : Python
    1995 : Java
         : PHP
    2009 : Go
    2012 : Julia
```

## Mindmap

```mermaid
mindmap
  root(("Procastination"))
    Coding
      Fixing the Bugs      
      Implement new Features        
    Sysadmin
      Updating Servers
      Check Logfiles
```

## Pie Chart

```mermaid
pie title A Pie Chart
    "Big Size" : 100
    "Middle Size" : 70
    "Small Size" : 30
```

## Git Graph

```mermaid
gitGraph
   commit
   commit
   commit
   branch "My Branch"
   checkout "My Branch"
   commit
   commit
   checkout main
   merge "My Branch"
   commit
   commit
   commit
```



